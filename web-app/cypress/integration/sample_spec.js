// Test d'exemple par défaut :
describe('The Traffic web site home page', () => {
    it('successfully loads', () => {
        cy.visit('/')
    })
})

describe('Scénario 1 ', () => {
    it('URL -> Configuration', () => {
        cy.get('a[href="#configuration"]').click()
        cy.url().should('include', 'configuration')
    })
})

describe('Scénario 2', () => {
    //On regarde si il y a bien 28 cases dans le tableau de segment
    it('28 segments on table', () => {
        cy.get('a[href="#configuration"]').click()
        cy.get('form[data-kind="segment"]').should('have.length',28)
    })
})

describe('Scénario 3', () => {
    it('Vehicule -> No vehicle available', () => {
        //On regarde si la table vehicule contient bien la phrase "No vehicle available"
        cy.get('table').contains("No vehicle available")
    })
})

describe('Scénario 4', () => {
    it('Click on config', () => {
        //On se met sur la page de configuration
        cy.get('a[href="#configuration"]').click()
    })
    it('Change speed seg', () => {
        //On change la speed du segment 5
        cy.get('input[name="speed"][form="segment-5"]').clear().type('30')
        //On appuie sur le bouton update du segment 5
        cy.get('#segment-5 > .btn').click()
        //On appuie sur le bouton de la pop up "close"
        cy.get('.modal-footer > .btn').click()
        //On regarde si la vitesse de la voiture est bien modifié sur le serveur de l'api
        cy.request ('GET','http://127.0.0.1:4567/elements')
            .should( (response) => {
            expect(response.body['segments'][4]).to.have.property('speed', 30)
        })
    })
})

describe('Scénario 5', () => {
    it('Click on config', () => {
        //On se met sur la page de configuration
        cy.get('a[href="#configuration"]').click()
    })
    it('Change round', () => {
        //On change la capicité du rond point à 4
        cy.get(':nth-child(3) > .card-body > form > :nth-child(2) > .form-control')
            .clear().type('4')
        //On change la durée du rond point à 15
        cy.get(':nth-child(3) > .card-body > form > :nth-child(3) > .form-control')
            .clear().type('15')
        //On appuie sur le bouton d'update
        cy.get(':nth-child(3) > .card-body > form > .btn').click()
        //On appuie sur le bouton de la pop up "close"
        cy.get('.modal-footer > .btn').click()
        //On regarde sur le serveur de l'api si les modifications ont bien été prise en compte
        cy.request ('GET','http://127.0.0.1:4567/elements')
            .should( (response) => {
                expect(response.body['crossroads'][2]).to.have.property('type', 'Roundabout')
                expect(response.body['crossroads'][2]).to.have.property('id', 31)
                expect(response.body['crossroads'][2]).to.have.property('capacity', 4)
                expect(response.body['crossroads'][2]).to.have.property('duration', 15)
            })
    })
})

describe('Scénario 6', () => {
    it('Click on config', () => {
        //On se met sur la page de configuration
        cy.get('a[href="#configuration"]').click()
    })
    it('Change trafficlight 29', () => {
        //On change la durée du feu orange à 4
        cy.get(':nth-child(1) > .card-body > form > :nth-child(2) > .form-control')
            .clear().type('4')
        //On change la durée du feu vert à 40
        cy.get(':nth-child(1) > .card-body > form > :nth-child(3) > .form-control')
            .clear().type('40')
        //On change next passage duration à 10
        cy.get(':nth-child(1) > .card-body > form > :nth-child(4) > .form-control')
            .clear().type('10')
        //On appuie sur le bouton d'update
        cy.get(':nth-child(1) > .card-body > form > .btn').click()
        //On appuie sur le bouton de la pop up "close"
        cy.get('.modal-footer >.btn').click()
        //On regarde si les modifications sont bien prise en compte
        cy.request ('GET','http://127.0.0.1:4567/elements')
            .should( (response) => {
                expect(response.body['crossroads'][0]).to.have.property('id', 29)
                expect(response.body['crossroads'][0]).to.have.property('type', 'TrafficLight')
                expect(response.body['crossroads'][0]).to.have.property('greenDuration', 40)
                expect(response.body['crossroads'][0]).to.have.property('nextPassageDuration', 10)
            })
    })
})


describe('Scénario 7', () => {
    it('Click on config', () => {
        //On se met sur la page de configuration
        cy.get('a[href="#configuration"]').click()
    })
    // Ajout des vehicules et de leur
    it('Add vehicle 1', () => {
        cy.get('form > :nth-child(1) > .form-control').clear().type('5')
        cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type('26')
        cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type('50')
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click()
    })
    it('Add vehicle 2', () => {
        cy.get('form > :nth-child(1) > .form-control').clear().type('19')
        cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type('8')
        cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type('200')
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click()
    })
    it('Add vehicle 3', () => {
        cy.get('form > :nth-child(1) > .form-control').clear().type('27')
        cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type('2')
        cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type('150')
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click()
    })

    //On regarde si les véhicules on une position = 0 si c'est le cas le véhicule est bien à l'arret
    it('Vehicles on server', () => {
        cy.request ('GET','http://127.0.0.1:4567/vehicles')
            .should((vehicles) => {
                expect(vehicles.body['50.0'][0]).to.have.property('position',0)
                expect(vehicles.body['150.0'][0]).to.have.property('position',0)
                expect(vehicles.body['200.0'][0]).to.have.property('position',0)
            })
    })
    it('Test vehicles on list', () => {
        //On regarde si le véhicule est bien dans la liste de véhicule
        cy.get('table').contains(200.0)
        cy.get('table').contains(150.0)
        cy.get('table').contains(50.0)
    })
})

describe('Scénario 8', () => {
    it('Click on simulation', () => {
        //On se met sur la page de simulation
        cy.get('a[href="#simulation"]').click()
    })
    it('Vehicles STOP', () => {
        //On regarde si l'icone est bien sur block
        cy.get(':nth-child(1) > :nth-child(6) > div > .material-icons').contains('block')
        cy.get(':nth-child(2) > :nth-child(6) > div > .material-icons').contains('block')
        cy.get(':nth-child(3) > :nth-child(6) > div > .material-icons').contains('block')
    })
    it('Run simulation => Progress BAR', () => {
        //On lance la simulation de 120
        cy.get('.form-control').clear().type('120')
        //On clique sur le bouton pour lancer la simulation
        cy.get('.btn').click()
        //On regarde dans la progess bar après un certain temps (ici le timeout se gère tout seul)
        // Et on regarde si le paramètre aria-valuenow est bien à 100 %
        cy.get('.progress-bar', { timeout: 50000 })
            .should('have.attr','aria-valuenow','100')
    })
    it('Verification if stop', () => {
        //On regarde si les 2 premier véhicules sont bien sur block
        cy.get(':nth-child(1) > :nth-child(6) > div > .material-icons').contains('block')
        cy.get(':nth-child(2) > :nth-child(6) > div > .material-icons').contains('block')
        //On regarde si le dernier véhicule est bien sur play_circle_filled càd en mouvement
        cy.get(':nth-child(3) > :nth-child(6) > div > .material-icons').contains('play_circle_filled')
    })
})

describe('Scénario 9', () => {

    it('Click on simulation', () => {
        //On se met sur la page simulation
        cy.get('a[href="#simulation"]').click()
    })

    it('Reset list of vehicles', () => {
        //On fait une requête pour reset la page
        cy.request('GET','http://localhost:3000/init')
        cy.reload()
    })

    it('Check vehicles is empty', () => {
        //On regarde si dans la table on a bien "No vehicle available"
        cy.get('table').contains("No vehicle available")
        cy.request ('GET','http://127.0.0.1:4567/vehicles')
            .should((vehicles) => {
                //Et on regarde aussi sur l'api la presence de vehicules
                if(!Object.keys(vehicles.body).length){
                    console.log("no data found");
                    cy.log("no data found")
                }
            })
    })

    it('Click on config', () => {
        //On se met sur la page de configuration
        cy.get('a[href="#configuration"]').click()
    })

    //On ajoute les differents vehicules avec leurs paramètres
    it('Add vehicle 1', () => {
        cy.get('form > :nth-child(1) > .form-control').clear().type('5')
        cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type('26')
        cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type('50')
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click()
    })
    it('Add vehicle 2', () => {
        cy.get('form > :nth-child(1) > .form-control').clear().type('19')
        cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type('8')
        cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type('200')
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click()
    })
    it('Add vehicle 3', () => {
        cy.get('form > :nth-child(1) > .form-control').clear().type('27')
        cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type('2')
        cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type('150')
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click()
    })

    it('Click on simulation', () => {
        //On se met sur la page de simulation
        cy.get('a[href="#simulation"]').click()
    })
    it('Run simulation => Progress BAR 100%', () => {
        //On lance une simulation de 500
        cy.get('.form-control').clear().type('500')
        cy.get('.btn').click()
        //On attend que la progress bar est bien à 100 % avec la paramètre aria-valuenow
        //Ici, on met un timeout très grand pour la pipeline
        cy.get('.progress-bar', { timeout: 500000 })
            .should('have.attr','aria-valuenow','100')
    })
    it('Check vehicles stop', () => {
        //On regarde si les vehicules ont bien l'icone block dans la case moving
        cy.get(':nth-child(1) > :nth-child(6) > div > .material-icons').contains('block')
        cy.get(':nth-child(2) > :nth-child(6) > div > .material-icons').contains('block')
        cy.get(':nth-child(3) > :nth-child(6) > div > .material-icons').contains('block')
    })
})


describe('Scénario 10', () => {

    it('Click on simulation', () => {
        //On se met sur la page de simulation
        cy.get('a[href="#simulation"]').click()
    })

    it('Reset list of vehicles', () => {
        //On reset la liste de vehicule
        cy.request('GET','http://localhost:3000/init')
        cy.reload()
    })
    it('Click on config', () => {
        //On se met sur la page configuration
        cy.get('a[href="#configuration"]').click()
    })
    //On ajoute les vehicules
    it('Add vehicle 1', () => {
        cy.get('form > :nth-child(1) > .form-control').clear().type('5')
        cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type('26')
        cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type('50')
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click()
    })
    it('Add vehicle 2', () => {
        cy.get('form > :nth-child(1) > .form-control').clear().type('5')
        cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type('26')
        cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type('80')
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click()
    })
    it('Add vehicle 3', () => {
        cy.get('form > :nth-child(1) > .form-control').clear().type('5')
        cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type('26')
        cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type('80')
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click()
    })

    it('Click on simulation', () => {
        //On se met sur la page de configuration
        cy.get('a[href="#simulation"]').click()
    })

    it('Run simulation => Progress BAR 100%', () => {
        //On lance une simulation de 200 et on regarde la progress bar
        cy.get('.form-control').clear().type('200')
        cy.get('.btn').click()
        cy.get('.progress-bar', { timeout: 50000 })
            .should('have.attr','aria-valuenow','100')
    })

    it('Vehicles on seg', () => {
        //On regarde si les vehicules sont bien sur les segments attendu
        cy.request ('GET','http://127.0.0.1:4567/vehicles')
            .should((vehicles) => {
                expect(vehicles.body['50.0'][0]['step']).to.have.property('id',17)
                expect(vehicles.body['80.0'][0]['step']).to.have.property('id',29)
                expect(vehicles.body['80.0'][0]['step']).to.have.property('id',29)
            })
    })
})